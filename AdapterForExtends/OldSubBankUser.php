<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 18:50
 */

namespace AdapterForExtends;

class OldSubBankUser
{
    private $name;
    private $savingsMain;
    private $savingsSub;

    public function __construct(string $name, int $savingsMain, int $savingsSub)
    {
        $this->name = $name;
        $this->savingsMain = $savingsMain;
        $this->savingsSub = $savingsSub;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSavings(): array
    {
        return [
            $this->savingsMain,
            $this->savingsSub
        ];
    }
}