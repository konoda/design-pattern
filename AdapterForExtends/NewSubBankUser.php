<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 18:55
 */

namespace AdapterForExtends;

class NewSubBankUser extends OldSubBankUser implements NewBankUserInterface
{
    public function getNewSavings(): int
    {
        return array_sum($this->getSavings());
    }
}