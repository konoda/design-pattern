<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 18:38
 */

namespace AdapterForExtends;

interface NewBankUserInterface
{
    public function getNewSavings(): int;
}