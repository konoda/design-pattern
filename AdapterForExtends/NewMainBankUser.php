<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 18:54
 */

namespace AdapterForExtends;

class NewMainBankUser extends OldMainBankUser implements NewBankUserInterface
{
    public function getNewSavings(): int
    {
        return $this->getSavings();
    }
}