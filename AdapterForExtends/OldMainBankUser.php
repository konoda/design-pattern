<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 18:39
 */

namespace AdapterForExtends;

class OldMainBankUser
{
    private $name;
    private $savings;

    public function __construct(string $name, int $savings)
    {
        $this->name = $name;
        $this->savings = $savings;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSavings()
    {
        return $this->savings;
    }
}