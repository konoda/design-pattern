<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 14:25
 */

namespace Iterator;

class Student
{
    /** string $name*/
    private $name;
    /** int $sex 1:man, 2:woman */
    private $sex;

    /**
     * Student constructor.
     *
     * @param string $name
     * @param int $sex
     */
    public function __construct(string $name, int $sex)
    {
        $this->name = $name;
        $this->sex = $sex;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getSex(): int
    {
        return $this->sex;
    }
}