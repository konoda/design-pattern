<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 14:25
 */

namespace Iterator;

class StudentList
{
    protected $list = [];
    private $last = 0;

    /**
     * @param \Iterator\Student $student
     */
    public function add(Student $student)
    {
        $this->list[$this->last] = $student;
        $this->list++;
    }

    /**
     * @param int $index
     * @return \Iterator\Student
     */
    public function getStudentAt(int $index): Student
    {
        return $this->list[$index];
    }

    /**
     * @return int
     */
    public function getLastNumber(): int
    {
        return $this->last;
    }
}