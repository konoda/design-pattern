<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 16:51
 */

namespace Iterator;

class MyStudentListIterator implements Iterator
{
    /** @var \Iterator\MyStudentList $myStudentLists */
    private $myStudentList;
    private $index;

    public function __construct(MyStudentList $myStudentList)
    {
        $this->myStudentList = $myStudentList;
        $this->index = 0;
    }

    public function next()
    {
        $student = $this->myStudentList->getStudentAt($this->index);
        $this->index++;

        return $student;
    }

    public function hasNext(): bool
    {
        return $this->myStudentList->getLastNumber() > $this->index;
    }
}