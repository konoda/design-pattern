<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 14:49
 */

namespace Iterator;

interface Teacher
{
    public function createStudentList();
    public function callStudents();
}