<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 17:06
 */

namespace Iterator;

class NewTeacher implements Teacher
{
    /** @var \Iterator\MyStudentList $studentList */
    protected $studentList;

    public function callStudents()
    {
        $itr = $this->studentList->iterator();
        while ($itr->hasNext()) {
            echo $itr->next()->getName();
        }
    }

    public function createStudentList()
    {
        $this->studentList = new MyStudentList();
        $this->studentList->add(new Student('masahiro', 1));
        $this->studentList->add(new Student('takuya', 1));
        $this->studentList->add(new Student('sora', 2));
    }
}