<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 16:42
 */

namespace Iterator;

interface Iterator
{
    public function hasNext(): bool;
    public function next();
}