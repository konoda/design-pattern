<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 16:48
 */

namespace Iterator;

interface Aggregate
{
    public function iterator(): Iterator;
}