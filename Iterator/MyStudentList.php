<?php
/**
 * Created by PhpStorm.
 * User: onoda
 * Date: 2017/12/26
 * Time: 16:45
 */

namespace Iterator;

class MyStudentList extends StudentList implements Aggregate
{
    public function iterator(): Iterator
    {
        return new MyStudentListIterator($this);
    }
}